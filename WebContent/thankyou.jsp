<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/Thankyou.java" --%><%-- /jsf:pagecode --%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ page 
language="java"
contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"
%>
<f:view>
	<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
	<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
	<%-- tpl:insert page="/theme/HTML-C-02_blue.htpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="${pageContext.request.contextPath}/theme/Master.css" rel="stylesheet" type="text/css">
<LINK href="${pageContext.request.contextPath}/theme/C_master_blue.css" rel="stylesheet" type="text/css">
<LINK href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" rel="stylesheet" type="text/css">
<SCRIPT language="JavaScript">

// method called on page load
function _initPage() {
	// insert site-wide code here


	// you typically would run this after all site-wide
	// execution has processed, but it's really up to
	// the design of your site to when you may want to run this
	if( typeof window.initPage == "function" ) {
		window.initPage();
	}

}

// method called on page unload
function _unloadPage(thisObj, thisEvent) {
	// insert site-wide code here


	// you typically would run this after all site-wide
	// execution has processed, but it's really up to
	// the design of your site to when you may want to run this
	if( typeof window.unloadPage == "function" ) {
		window.unloadPage(thisObj, thisEvent);
	}

}

// method called on page unload
function _onbeforeunloadPage(thisObj, thisEvent) {
	// insert site-wide code here


	// you typically would run this after all site-wide
	// execution has processed, but it's really up to
	// the design of your site to when you may want to run this
	if( typeof window.onbeforeunloadPage == "function" ) {
		window.onbeforeunloadPage(thisObj, thisEvent);
	}

}

</SCRIPT>
<%-- tpl:put name="htmlheadarea" --%>
	<TITLE>USA TODAY System Access Request Complete</TITLE>

			<LINK rel="stylesheet" type="text/css" href="theme/stylesheet.css"
				title="Style">
		<%-- /tpl:put --%>
</HEAD>
<BODY onLoad="_initPage();" 
      onUnload="_unloadPage(this, event);"
      onbeforeunload="_onbeforeunloadPage(this, event);">
<!-- start header area -->
<div class="topAreaBox"></div>
<div class="topAreaLogo"><IMG border="0" src="${pageContext.request.contextPath}/theme/images/800x125_top.jpg" width="800"
	height="125" usemap="/bizreg#800x125_top"><MAP name="800x125_top">
	<AREA shape="rect" href="/bizreg/index.jsp" coords="19,17,265,113">
	<AREA shape="default" nohref="nohref">
</MAP>
</div>
<div class="topAreaLogoLine"><IMG border="0" src="${pageContext.request.contextPath}/theme/images/800x20_art2.jpg" height="20" width="800"></div>
<!-- end header area -->

<TABLE>
	<tbody>
  		<tr>
  			<td colspan="2" height="135">&nbsp;</td>
  		</tr>
  		<tr>
<!-- start main content area -->
			<td>&nbsp;&nbsp;</td>
			<td align="left" valign="top">
					<%-- tpl:put name="bodyarea" --%><hx:scriptCollector id="scriptCollector1">
						<BR>
						<CENTER>
						<h:form styleClass="form" id="form1"><TABLE border="0" cellpadding="2" cellspacing="2" width="700">
							<TBODY>
								<TR>
									<TD><SPAN style="font-weight: bold; font-size: 12pt">Your request for system access has been saved. USA TODAY will review the request.  This is an initial sign up period.  We will coordinate the activation date with your corporate headquarters. </SPAN><BR>
											<BR>
											<B>Important: Please do not use your browser's Back Button to
											enter a new request. Click the button below.</B><BR>
										<BR>
											<hx:commandExButton type="submit"
												value="Enter New System Access Request"
												styleClass="commandExButton" id="buttonEnterNewRequest" action="#{pc_Thankyou.doButtonEnterNewRequestAction}"></hx:commandExButton>
										<BR>
										<h:outputText styleClass="outputText" id="textUniqueRequest" value="#{lastUnique.unique}" style="color: white"><f:convertNumber /></h:outputText>
										<BR>
									</TD>
								</TR>
								<TR>
									<TD><SPAN style="font-size: 10pt">Please ensure that your Junk Email filters/Spam filters are not
									blocking any emails from <U><FONT color="blue">AS400SERVICES@USATODAY.COM</FONT></U> to assure you receive your activation e-mail.</SPAN></TD>
								</TR>
								<TR>
									<TD></TD>
								</TR>
								<TR>
									<TD><hx:outputLinkEx styleClass="outputLinkEx" value="/bizreg/index.usat" id="linkEx1">
											<h:outputText id="textLinkLabel" styleClass="outputText" value="Click Here to enter additional access requests"></h:outputText>
										</hx:outputLinkEx></TD>
								</TR>
							</TBODY>
						</TABLE></h:form>
						</CENTER>
						<BR><BR>
						<BR><BR></hx:scriptCollector><%-- /tpl:put --%>
<!-- end main content area -->
           </td>
       </tr>
		<TR>
			<TD align="left" valign="top" colspan="2" height="20"><img class="footerImage" src="${pageContext.request.contextPath}/theme/images/800x20_art2.jpg" /></TD></TR>
		<TR>
			<TD align="center" valign="top"><DIV class="footer">
				</DIV>
			</TD>
			<TD class="mainContentWideTD" align="center" valign="top">
			<div class="footer">
				<table border="0" cellspacing="0" cellpadding="0" width="675">
					<tbody>
						<tr>
						<td class="mainContentWideTD" align="center"><!-- someting in this table drastically impacts how this template works so leave it in for now --></td>
						</tr>
				</tbody></table>
				</div>
				
				</TD>
		</TR>
		<TR>
			<TD valign="top" colspan="2">
				<div class="footer">
				<table border="0" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
						<td>
							<img src="${pageContext.request.contextPath}/theme/1x1.gif" hspace="15">
						</td>
						<td class="mainContentWideTD" align="center">&copy; Copyright 2007 <A
							href="http://www.usatoday.com/marketing/credit.htm" target="_blank">USA
						TODAY</A>, a division of <A
						href="http://www.gannett.com/bizreg" target="_blank">Gannett
						Co.</A> Inc. <A href="http://service.usatoday.com/scweb/legal/privacy.html" target="_blank">Privacy
						Policy</A>, 
						By using this
						service, you accept our <A href="http://service.usatoday.com/scweb/legal/service.html" target="_blank">Terms of Service.</A>								
						</td>
						</tr>
				</tbody></table>
				</div>
			</TD>
		</TR>
	</tbody>
</table>
</BODY>
</HTML><%-- /tpl:insert --%>
</f:view>