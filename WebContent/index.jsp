<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/Index.java" --%><%-- /jsf:pagecode --%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<f:view>
<%-- tpl:insert page="/theme/HTML-C-02_blue.htpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="${pageContext.request.contextPath}/theme/Master.css" rel="stylesheet" type="text/css">
<LINK href="${pageContext.request.contextPath}/theme/C_master_blue.css" rel="stylesheet" type="text/css">
<LINK href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" rel="stylesheet" type="text/css">
<SCRIPT language="JavaScript">

// method called on page load
function _initPage() {
	// insert site-wide code here


	// you typically would run this after all site-wide
	// execution has processed, but it's really up to
	// the design of your site to when you may want to run this
	if( typeof window.initPage == "function" ) {
		window.initPage();
	}

}

// method called on page unload
function _unloadPage(thisObj, thisEvent) {
	// insert site-wide code here


	// you typically would run this after all site-wide
	// execution has processed, but it's really up to
	// the design of your site to when you may want to run this
	if( typeof window.unloadPage == "function" ) {
		window.unloadPage(thisObj, thisEvent);
	}

}

// method called on page unload
function _onbeforeunloadPage(thisObj, thisEvent) {
	// insert site-wide code here


	// you typically would run this after all site-wide
	// execution has processed, but it's really up to
	// the design of your site to when you may want to run this
	if( typeof window.onbeforeunloadPage == "function" ) {
		window.onbeforeunloadPage(thisObj, thisEvent);
	}

}

</SCRIPT>
<%-- tpl:put name="htmlheadarea" --%>
				
<%
	// com.gannett.usatoday.SCWebRegistration.handler.UniqueRequestHandler urh = new com.gannett.usatoday.SCWebRegistration.handler.UniqueRequestHandler();
	// session.removeAttribute("uniqueRequest");
	// session.setAttribute("uniqueRequest", urh);
 %>
 <META HTTP-EQUIV="expires" CONTENT="0">
 
			<TITLE>USA TODAY B2B System Access Request</TITLE>
					<LINK rel="stylesheet" type="text/css" href="theme/stylesheet.css" title="Style">
				<SCRIPT type="text/javascript">
var formSubmitted = false;

function initPage() {
	var hiddenField = document.getElementById('form1:hiddenUniqueRequestNumber');
	var d = new Date();

	hiddenField.value = d.getTime();
}

function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
return false;
}
function func_2(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
if (!formSubmitted) {
	formSubmitted = true;
	thisObj.value = "Please Wait...";
	return true;
}

return false;
}
function func_3(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
formSubmitted = true;
}</SCRIPT>
			
		<%-- /tpl:put --%>
</HEAD>
<BODY onLoad="_initPage();" 
      onUnload="_unloadPage(this, event);"
      onbeforeunload="_onbeforeunloadPage(this, event);">
<!-- start header area -->
<div class="topAreaBox"></div>
<div class="topAreaLogo"><IMG border="0" src="${pageContext.request.contextPath}/theme/images/800x125_top.jpg" width="800"
	height="125" usemap="/bizreg#800x125_top"><MAP name="800x125_top">
	<AREA shape="rect" href="/bizreg/index.jsp" coords="19,17,265,113">
	<AREA shape="default" nohref="nohref">
</MAP>
</div>
<div class="topAreaLogoLine"><IMG border="0" src="${pageContext.request.contextPath}/theme/images/800x20_art2.jpg" height="20" width="800"></div>
<!-- end header area -->

<TABLE>
	<tbody>
  		<tr>
  			<td colspan="2" height="135">&nbsp;</td>
  		</tr>
  		<tr>
<!-- start main content area -->
			<td>&nbsp;&nbsp;</td>
			<td align="left" valign="top">
					<%-- tpl:put name="bodyarea" --%><hx:scriptCollector id="scriptCollector1">
								<BR>
								<br/>
								<br/><div style="width: 700px;">
							<span
								style='font-size: 14.0pt; font-family: "Calibri", "sans-serif"; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA'>USA TODAY is no longer using this site for hotel registrations, but we are happy to enroll your property in our online newspaper ordering program by placing a phone call to our National Business Operations Center at <b>704-527-4504 or
									800-628-6180.</b>&nbsp;<br><br><br>Thank you!</span>
									</div>
								<br/><div style="height: 2px; background-color: black; width: 680px"></div>
								<br/>
<!-- 			<br/>
								<br/>
								<br/>
								<br/>
								<br/>
								<br/>
								<br/>
								<br/>
	 -->											

							<div id= formDiv style="display:none">
									<h:form styleClass="form" id="form1" onsubmit="return func_3(this, event);"><TABLE width="675" cellspacing="0" cellpadding="2">
	<TBODY>
										<TR>
											<TD align="left"></TD>
											<TD align="left" colspan="4"><SPAN style="font-size: 10pt"><B>USA TODAY offers a web based application for hotel properties to order papers on a daily/weekly basis.  To sign up, complete the form below and click the 'Submit Request' button. You will receive an email when your account is activated. USA TODAY may need to contact you regarding your access request.  Any information gathered will strictly be used by USA TODAY for account maintenance only.<BR>
											<BR>
											<SPAN style="color: #7e93cb; text-decoration: underline;">Hotel eligibility
											requirement:</SPAN>
											The hotel changes order quantities daily or weekly. </B></SPAN></TD>
										</TR>
										<TR>
											<TD align="left"></TD>
											<TD align="left" colspan="2"></TD>
											<TD align="left"></TD>
											<TD align="left"></TD>
										</TR>
										<TR>
											<TD align="left"></TD>
											<TD align="left" colspan="3"><h:messages
												styleClass="messages" id="messages1" globalOnly="true"></h:messages></TD>
											<TD align="left"></TD>
										</TR>
										<TR>
											<TD align="left"></TD>
											<TD align="left" colspan="4" bgcolor="#e8e8e8"><FONT
												color="black" size="+0"><B>::&nbsp;Location Information ::</B></FONT></TD>
										</TR>
										<TR>
											<TD align="left"></TD>
											<TD align="right" nowrap></TD>
											<TD style="width:5px"></TD>
											<TD width="200" colspan="2"><h:outputText
												styleClass="outputText" id="text14"
												value='"*" Denotes A Required Field'
												style="font-size: 12px; font-weight: bold"></h:outputText>
													</TD></TR>
										<TR>
											<TD align="left"></TD>
											<TD align="right" nowrap><B>*</B> Authorization Code:</TD>
	<TD style="width:5px">&nbsp;</TD>
	<TD width="200">
<h:inputText id="textAuthCode"
												styleClass="inputText" required="true" size="30" value="#{accessRequest.authorizationCode}" maxlength="20">
												<f:validateLength maximum="20" minimum="1"></f:validateLength>
											</h:inputText><h:message styleClass="message" id="message2"
												for="textAuthCode"></h:message>
							</TD>
											<TD align="left" nowrap>(The code provided to you by USA TODAY via your Corp. Communication)</TD>
										</TR>
<TR>
											<TD align="left"></TD>
											<TD align="right"><B>*</B> Location Name:</TD>
	<TD style="width:5px">&nbsp;</TD>
	<TD width="200"><h:inputText id="textLocationName"
												styleClass="inputText" required="true" size="30"
												value="#{accessRequest.locationName}" maxlength="45">
												<f:validateLength maximum="50" minimum="1"></f:validateLength>
											</h:inputText><h:message styleClass="message" id="message3"
												for="textLocationName"></h:message>
</TD>
											<TD align="left"></TD>
										</TR>
<TR>
											<TD align="left"></TD>
											<TD align="right"><B>*</B> Property Code Or Number:</TD>
	<TD style="width:5px">&nbsp;</TD>
	<TD width="200"><h:inputText id="textLocationID"
												styleClass="inputText" size="30" maxlength="20" value="#{accessRequest.locationID}" required="true">
												<f:validateLength maximum="20" minimum="1"></f:validateLength>
											</h:inputText><h:message styleClass="message" id="message4"
												for="textLocationID"></h:message>
</TD>
											<TD align="left"></TD>
										</TR>
<TR>
											<TD align="left"></TD>
											<TD align="right"><B>*</B> Street Address 1:</TD>
	<TD style="width:5px">&nbsp;</TD>
	<TD width="200"><h:inputText id="textLocationAddress1"
												styleClass="inputText" size="30" value="#{accessRequest.locationAddress1}" maxlength="45" required="true">
												<f:validateLength maximum="45" minimum="1"></f:validateLength>
											</h:inputText><h:message styleClass="message" id="message5"
												for="textLocationAddress1"></h:message>
</TD>
											<TD align="left"></TD>
										</TR>
										<TR>
											<TD align="left"></TD>
											<TD align="right"><B>*</B> City:</TD>
											<TD style="width:5px"></TD>
											<TD width="200"><h:inputText styleClass="inputText" id="textCity" maxlength="50" size="30" value="#{accessRequest.locationCity}" required="true">
												<f:validateLength maximum="50" minimum="1"></f:validateLength>
											</h:inputText><h:message
												styleClass="message" id="message11" for="textCity"></h:message></TD>
											<TD align="left"></TD>
										</TR>
										<TR>
											<TD align="left"></TD>
											<TD align="right"><B>*</B>  State:</TD>
											<TD style="width:5px"></TD>
											<TD width="200"><h:selectOneListbox styleClass="selectOneListbox"
												id="listboxState" size="1" value="#{accessRequest.locationState}">
												<f:selectItem itemValue="none selected" itemLabel=" " />
												<f:selectItem itemValue="Alabama" itemLabel="Alabama" />
														<f:selectItem itemValue="Alaska" itemLabel="Alaska" />
														<f:selectItem itemValue="Arizona" itemLabel="Arizona" />
														<f:selectItem itemValue="Arkansas" itemLabel="Arkansas" />
														<f:selectItem itemValue="California" itemLabel="California" />
														<f:selectItem itemValue="Colorado" itemLabel="Colorado" />
														<f:selectItem itemValue="Connecticut" itemLabel="Connecticut" />
														<f:selectItem itemValue="Delaware" itemLabel="Delaware" />
														<f:selectItem itemValue="District of Columbia" itemLabel="District of Columbia" />
														<f:selectItem itemValue="Florida" itemLabel="Florida" />
														<f:selectItem itemValue="Georgia" itemLabel="Georgia" />
														<f:selectItem itemValue="Hawaii" itemLabel="Hawaii" />
														<f:selectItem itemValue="Idaho" itemLabel="Idaho" />
														<f:selectItem itemValue="Illinois" itemLabel="Illinois" />
														<f:selectItem itemValue="Indiana" itemLabel="Indiana" />
														<f:selectItem itemValue="Iowa" itemLabel="Iowa" />
														<f:selectItem itemValue="Kansas" itemLabel="Kansas" />
														<f:selectItem itemValue="Kentucky" itemLabel="Kentucky" />
														<f:selectItem itemValue="Louisiana" itemLabel="Louisiana" />
														<f:selectItem itemValue="Maine" itemLabel="Maine" />
														<f:selectItem itemValue="Maryland" itemLabel="Maryland" />
														<f:selectItem itemValue="Massachusetts" itemLabel="Massachusetts" />
														<f:selectItem itemValue="Michigan" itemLabel="Michigan" />
														<f:selectItem itemValue="Minnesota" itemLabel="Minnesota" />
														<f:selectItem itemValue="Mississippi" itemLabel="Mississippi" />
														<f:selectItem itemValue="Missouri" itemLabel="Missouri" />
														<f:selectItem itemValue="Montana" itemLabel="Montana" />
														<f:selectItem itemValue="Nebraska" itemLabel="Nebraska" />
														<f:selectItem itemValue="Nevada" itemLabel="Nevada" />
														<f:selectItem itemValue="New Hampshire" itemLabel="New Hampshire" />
														<f:selectItem itemValue="New Jersey" itemLabel="New Jersey" />
														<f:selectItem itemValue="New Mexico" itemLabel="New Mexico" />
														<f:selectItem itemValue="New York" itemLabel="New York" />
														<f:selectItem itemValue="North Carolina" itemLabel="North Carolina" />
														<f:selectItem itemValue="North Dakota" itemLabel="North Dakota" />
														<f:selectItem itemValue="Ohio" itemLabel="Ohio" />
														<f:selectItem itemValue="Oklahoma" itemLabel="Oklahoma" />
														<f:selectItem itemValue="Oregon" itemLabel="Oregon" />
														<f:selectItem itemValue="Pennsylvania" itemLabel="Pennsylvania" />
														<f:selectItem itemValue="Rhode Island" itemLabel="Rhode Island" />
														<f:selectItem itemValue="South Carolina" itemLabel="South Carolina" />
														<f:selectItem itemValue="South Dakota" itemLabel="South Dakota" />
														<f:selectItem itemValue="Tennessee" itemLabel="Tennessee" />
														<f:selectItem itemValue="Texas" itemLabel="Texas" />
														<f:selectItem itemValue="Utah" itemLabel="Utah" />
														<f:selectItem itemValue="Vermont" itemLabel="Vermont" />
														<f:selectItem itemValue="Virginia" itemLabel="Virginia" />
														<f:selectItem itemValue="Washington" itemLabel="Washington" />
														<f:selectItem itemValue="West Virginia" itemLabel="West Virginia" />
														<f:selectItem itemValue="Wisconsin" itemLabel="Wisconsin" />
														<f:selectItem itemValue="Wyoming" itemLabel="Wyoming" />
											</h:selectOneListbox></TD>
											<TD align="left"></TD>
										</TR>
										<TR>
											<TD align="left"></TD>
											<TD align="right"><B>*</B> Zip:</TD>
											<TD style="width:5px"></TD>
											<TD width="200"><h:inputText styleClass="inputText" id="textZip" maxlength="10"
												size="15" value="#{accessRequest.locationZip}" required="true">
												<f:validateLength maximum="10" minimum="5"></f:validateLength>
											</h:inputText><h:message styleClass="message" id="message12"
												for="textZip"></h:message></TD>
											<TD align="left"></TD>
										</TR>
										<TR>
											<TD align="left">&nbsp;</TD>
											<TD align="right"></TD>
											<TD style="width:5px"></TD>
											<TD width="200"></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD align="left"></TD>
											<TD align="left" colspan="4" bgcolor="#e8e8e8"><TABLE width="100%" border="0" cellpadding="0" cellspacing="1">
												<TBODY>
													<TR bgcolor="#e8e8e8">
														<TD colspan="2"><FONT color="black" size="-1"><hx:outputSeparator
															styleClass="outputSeparator" id="separator1" color="#ece9d8"></hx:outputSeparator>You may add 2 users per location. If you require more than 2 users, please complete another form. A separate form is required for each location.</FONT><hx:outputSeparator
															styleClass="outputSeparator" id="separator2" color="#ece9d8"></hx:outputSeparator></TD></TR>
													<TR>
														<TD colspan="2"><FONT color="black" size="+0"><B>:: Required User
														Information ::</B></FONT></TD></TR>
												</TBODY>
											</TABLE>
											</TD>
										</TR>
										<TR>
											<TD align="left"></TD>
											<TD align="right" nowrap><B>*</B> First and Last Name:</TD>
											<TD style="width:5px"></TD>
											<TD width="200"><h:inputText id="textName1"
												styleClass="inputText" required="true" size="30"
												value="#{accessRequest.name1}" maxlength="50">
												<f:validateLength maximum="50" minimum="1"></f:validateLength>
											</h:inputText><h:message styleClass="message" id="message1"
												for="textName1"></h:message></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD align="left"></TD>
											<TD align="right"><B>*</B> Phone:</TD>
	<TD style="width:5px">&nbsp;</TD>
	<TD width="200"><h:inputText styleClass="inputText" id="textPhone1"
												size="30" required="true" maxlength="15" value="#{accessRequest.phone1}">
												<f:validateLength maximum="15" minimum="7"></f:validateLength>
											</h:inputText><h:message
												styleClass="message" id="message6" for="textPhone1"></h:message></TD>
											<TD></TD>
										</TR>
<TR>
											<TD align="left"></TD>
											<TD align="right"><B>*</B> Business Email Address:</TD>
	<TD style="width:5px">&nbsp;</TD>
	<TD width="200"><h:inputText id="textEmail1"
												styleClass="inputText" required="true" maxlength="50" size="30" value="#{accessRequest.email1}">
												<f:validateLength maximum="50" minimum="1"></f:validateLength>
											</h:inputText><h:message styleClass="message" id="message7"
												for="textEmail1"></h:message>
</TD>
											<TD></TD>
										</TR>
										<TR>
											<TD align="left"></TD>
											<TD align="right"></TD>
											<TD style="width:5px"></TD>
											<TD width="200"></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD align="left"></TD>
											<TD align="left" colspan="3" bgcolor="#e8e8e8"><FONT
												color="black" size="+0"><B>:: Optional User Information ::</B></FONT></TD>
											<TD bgcolor="#e8e8e8">(Optional,  USA TODAY recommends at least one backup)</TD>
										</TR>
										<TR>
											<TD align="left"></TD>
											<TD align="right">First and Last Name:</TD>
											<TD style="width:5px"></TD>
											<TD width="200"><h:inputText styleClass="inputText"
												id="textName2" size="30" maxlength="50" value="#{accessRequest.name2}">
												<f:validateLength maximum="50"></f:validateLength>
											</h:inputText><h:message styleClass="message" id="message13"
												for="textName2"></h:message></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD align="left"></TD>
											<TD align="right">Phone:</TD>
											<TD style="width:5px"></TD>
											<TD width="200"><h:inputText styleClass="inputText"
												id="textPhone2" size="30"  maxlength="15" value="#{accessRequest.phone2}">
												<f:validateLength maximum="15"></f:validateLength>
											</h:inputText><h:message styleClass="message" id="message14"
												for="textPhone2"></h:message></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD align="left"></TD>
											<TD align="right">Business Email Address:</TD>
	<TD style="width:5px">&nbsp;</TD>
	<TD width="200"><h:inputText id="textEmail2"
												styleClass="inputText" size="30"  maxlength="50" value="#{accessRequest.email2}">
												<f:validateLength maximum="50"></f:validateLength>
											</h:inputText><h:message styleClass="message" id="message8"
												for="textEmail2"></h:message>
</TD>
											<TD></TD>
										</TR>
										<TR>
											<TD align="left"></TD>
											<TD align="right"></TD>
											<TD style="width:5px"></TD>
											<TD width="200"><h:inputHidden id="hiddenUniqueRequestNumber" value="#{uniqueRequest.unique}"><f:convertNumber /></h:inputHidden></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD align="left"></TD>
											<TD align="right"></TD>
											<TD style="width:5px"></TD>
											<TD width="200"><hx:commandExButton
												id="buttonSaveAccessRequest" styleClass="commandExButton"
												type="submit" value="Submit Request"
												action="#{pc_Index.doButtonSaveAccessRequestAction}" ondblclick="return func_1(this, event);" onclick="return func_2(this, event);">
											</hx:commandExButton></TD>
											<TD></TD>
										</TR>
									</TBODY>
</TABLE>
<BR>
								
							</h:form>
							</div>
									<BR></hx:scriptCollector><%-- /tpl:put --%>
<!-- end main content area -->
           </td>
       </tr>
		<TR>
			<TD align="left" valign="top" colspan="2" height="20"><img class="footerImage" src="${pageContext.request.contextPath}/theme/images/800x20_art2.jpg" /></TD></TR>
		<TR>
			<TD align="center" valign="top"><DIV class="footer">
				</DIV>
			</TD>
			<TD class="mainContentWideTD" align="center" valign="top">
			<div class="footer">
				<table border="0" cellspacing="0" cellpadding="0" width="675">
					<tbody>
						<tr>
						<td class="mainContentWideTD" align="center"><!-- someting in this table drastically impacts how this template works so leave it in for now --></td>
						</tr>
				</tbody></table>
				</div>
				
				</TD>
		</TR>
		<TR>
			<TD valign="top" colspan="2">
				<div class="footer">
				<table border="0" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
						<td>
							<img src="${pageContext.request.contextPath}/theme/1x1.gif" hspace="15">
						</td>
						<td class="mainContentWideTD" align="center">&copy; Copyright 2007 <A
							href="http://www.usatoday.com/marketing/credit.htm" target="_blank">USA
						TODAY</A>, a division of <A
						href="http://www.gannett.com/bizreg" target="_blank">Gannett
						Co.</A> Inc. <A href="http://service.usatoday.com/scweb/legal/privacy.html" target="_blank">Privacy
						Policy</A>, 
						By using this
						service, you accept our <A href="http://service.usatoday.com/scweb/legal/service.html" target="_blank">Terms of Service.</A>								
						</td>
						</tr>
				</tbody></table>
				</div>
			</TD>
		</TR>
	</tbody>
</table>
</BODY>
</HTML><%-- /tpl:insert --%>
</f:view>