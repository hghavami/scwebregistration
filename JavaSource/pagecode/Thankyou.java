/*
 * Created on Jun 26, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package pagecode;

import com.ibm.faces.component.html.HtmlScriptCollector;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlForm;
import com.ibm.faces.component.html.HtmlCommandExButton;
/**
 * @author aeast
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Thankyou extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlOutputLinkEx linkEx1;
	protected HtmlOutputText textLinkLabel;
	protected HtmlOutputText textUniqueRequest;
	protected HtmlForm form1;
	protected HtmlCommandExButton buttonEnterNewRequest;
	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}
	protected HtmlOutputLinkEx getLinkEx1() {
		if (linkEx1 == null) {
			linkEx1 = (HtmlOutputLinkEx) findComponentInRoot("linkEx1");
		}
		return linkEx1;
	}
	protected HtmlOutputText getTextLinkLabel() {
		if (textLinkLabel == null) {
			textLinkLabel = (HtmlOutputText) findComponentInRoot("textLinkLabel");
		}
		return textLinkLabel;
	}
	protected HtmlOutputText getTextUniqueRequest() {
		if (textUniqueRequest == null) {
			textUniqueRequest = (HtmlOutputText) findComponentInRoot("textUniqueRequest");
		}
		return textUniqueRequest;
	}
	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}
	protected HtmlCommandExButton getButtonEnterNewRequest() {
		if (buttonEnterNewRequest == null) {
			buttonEnterNewRequest = (HtmlCommandExButton) findComponentInRoot("buttonEnterNewRequest");
		}
		return buttonEnterNewRequest;
	}
	public String doButtonEnterNewRequestAction() {
		// Type Java code that runs when the component is clicked

		// TODO: Return outcome that corresponds to a navigation rule
		return "success";
	}
}