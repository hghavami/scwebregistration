/*
 * Created on Jun 20, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package pagecode;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.gannett.usatoday.SCWebRegistration.integration.SCAccessRequestDAO;
import com.gannett.usatoday.SCWebRegistration.to.AccessRequestTO;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.gannett.usatoday.SCWebRegistration.handler.UniqueRequestHandler;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneListbox;
import com.ibm.faces.component.html.HtmlOutputSeparator;
import javax.faces.component.html.HtmlInputHidden;
import com.ibm.faces.component.html.HtmlCommandExButton;
import javax.faces.component.html.HtmlMessage;
/**
 * @author aeast
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Index extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected AccessRequestTO accessRequest;
	protected UniqueRequestHandler uniqueRequest;
	protected UniqueRequestHandler lastUnique;
	protected HtmlForm form1;
	protected HtmlMessages messages1;
	protected HtmlOutputText text14;
	protected HtmlInputText textAuthCode;
	protected HtmlInputText textLocationName;
	protected HtmlInputText textLocationID;
	protected HtmlInputText textLocationAddress1;
	protected HtmlInputText textCity;
	protected HtmlSelectOneListbox listboxState;
	protected HtmlInputText textZip;
	protected HtmlOutputSeparator separator1;
	protected HtmlInputText textName1;
	protected HtmlInputText textPhone1;
	protected HtmlInputText textEmail1;
	protected HtmlInputText textName2;
	protected HtmlInputText textPhone2;
	protected HtmlInputText textEmail2;
	protected HtmlInputHidden hiddenUniqueRequestNumber;
	protected HtmlCommandExButton buttonSaveAccessRequest;
	protected HtmlMessage message2;
	protected HtmlMessage message3;
	protected HtmlMessage message4;
	protected HtmlMessage message5;
	protected HtmlMessage message11;
	protected HtmlMessage message12;
	protected HtmlOutputSeparator separator2;
	protected HtmlMessage message1;
	protected HtmlMessage message6;
	protected HtmlMessage message7;
	protected HtmlMessage message13;
	protected HtmlMessage message14;
	protected HtmlMessage message8;
	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}
	public String doButtonSaveAccessRequestAction() {
		// Type Java code that runs when the component is clicked

		try {
			AccessRequestTO to = this.getAccessRequest();
		
			SCAccessRequestDAO dao = new SCAccessRequestDAO();

	        //Long lastUnique = (Long)this.getSessionScope().get("LastUnique");
	        //com.gannett.usatoday.SCWebRegistration.handler.UniqueRequestHandler requestUnique = (com.gannett.usatoday.SCWebRegistration.handler.UniqueRequestHandler)this.getSessionScope().get("uniqueRequest");
	        com.gannett.usatoday.SCWebRegistration.handler.UniqueRequestHandler requestUnique = this.getUniqueRequest();
	        com.gannett.usatoday.SCWebRegistration.handler.UniqueRequestHandler lastUniqueRequest = this.getLastUnique();
	        
//	        System.out.println("Request Unique: " + requestUnique.getUnique().toString());
	        //System.out.println("Last Unique: " + lastUniqueRequest.getUnique().toString());
	        
	        if (lastUniqueRequest != null && requestUnique.getUnique().equals(lastUniqueRequest.getUnique())) {
				// don't resave if they've clicked multiple times on same request. Or refreshed the complete page.
				System.out.println("BizReg Index.java.Savebutton() - Multi Form Submission (Refresh button) Detected: ");
				
	            FacesContext context = FacesContext.getCurrentInstance();
	            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Please do not use your browsers Back or Refresh button while entering requests. Please use the buttons provided on the page.", null);
	            context.addMessage(null, message);
	        	
				return "startOver";
	        }
	        
			String clientIP = "";
			HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
			clientIP = request.getRemoteAddr();
			to.setClientIP(clientIP);
			
			dao.insertAccessRequest(to);
			
			lastUnique.setUnique(requestUnique.getUnique());
			
		} catch (Exception e) {
			
			System.out.println("BizReg Index.java.Savebutton() - Failed to Save Record: " + e.getMessage());
			
            FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, e.getMessage(), null);
            context.addMessage(null, message);
            return "failure";
			
		}
		
		return "success";
	}
	/** 
	 * @managed-bean true
	 */
	protected AccessRequestTO getAccessRequest() {
		if (accessRequest == null) {
			accessRequest = (AccessRequestTO) getManagedBean("accessRequest");
		}
		return accessRequest;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setAccessRequest(AccessRequestTO accessRequest) {
		this.accessRequest = accessRequest;
	}
	/** 
	 * @managed-bean true
	 */
	protected UniqueRequestHandler getUniqueRequest() {
		if (uniqueRequest == null) {
			uniqueRequest = (UniqueRequestHandler) getManagedBean("uniqueRequest");
		}
		return uniqueRequest;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setUniqueRequest(UniqueRequestHandler uniqueRequest) {
		this.uniqueRequest = uniqueRequest;
	}
	/** 
	 * @managed-bean true
	 */
	protected UniqueRequestHandler getLastUnique() {
		if (lastUnique == null) {
			lastUnique = (UniqueRequestHandler) getManagedBean("lastUnique");
		}
		return lastUnique;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setLastUnique(UniqueRequestHandler lastUnique) {
		this.lastUnique = lastUnique;
	}
	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}
	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}
	protected HtmlOutputText getText14() {
		if (text14 == null) {
			text14 = (HtmlOutputText) findComponentInRoot("text14");
		}
		return text14;
	}
	protected HtmlInputText getTextAuthCode() {
		if (textAuthCode == null) {
			textAuthCode = (HtmlInputText) findComponentInRoot("textAuthCode");
		}
		return textAuthCode;
	}
	protected HtmlInputText getTextLocationName() {
		if (textLocationName == null) {
			textLocationName = (HtmlInputText) findComponentInRoot("textLocationName");
		}
		return textLocationName;
	}
	protected HtmlInputText getTextLocationID() {
		if (textLocationID == null) {
			textLocationID = (HtmlInputText) findComponentInRoot("textLocationID");
		}
		return textLocationID;
	}
	protected HtmlInputText getTextLocationAddress1() {
		if (textLocationAddress1 == null) {
			textLocationAddress1 = (HtmlInputText) findComponentInRoot("textLocationAddress1");
		}
		return textLocationAddress1;
	}
	protected HtmlInputText getTextCity() {
		if (textCity == null) {
			textCity = (HtmlInputText) findComponentInRoot("textCity");
		}
		return textCity;
	}
	protected HtmlSelectOneListbox getListboxState() {
		if (listboxState == null) {
			listboxState = (HtmlSelectOneListbox) findComponentInRoot("listboxState");
		}
		return listboxState;
	}
	protected HtmlInputText getTextZip() {
		if (textZip == null) {
			textZip = (HtmlInputText) findComponentInRoot("textZip");
		}
		return textZip;
	}
	protected HtmlOutputSeparator getSeparator1() {
		if (separator1 == null) {
			separator1 = (HtmlOutputSeparator) findComponentInRoot("separator1");
		}
		return separator1;
	}
	protected HtmlInputText getTextName1() {
		if (textName1 == null) {
			textName1 = (HtmlInputText) findComponentInRoot("textName1");
		}
		return textName1;
	}
	protected HtmlInputText getTextPhone1() {
		if (textPhone1 == null) {
			textPhone1 = (HtmlInputText) findComponentInRoot("textPhone1");
		}
		return textPhone1;
	}
	protected HtmlInputText getTextEmail1() {
		if (textEmail1 == null) {
			textEmail1 = (HtmlInputText) findComponentInRoot("textEmail1");
		}
		return textEmail1;
	}
	protected HtmlInputText getTextName2() {
		if (textName2 == null) {
			textName2 = (HtmlInputText) findComponentInRoot("textName2");
		}
		return textName2;
	}
	protected HtmlInputText getTextPhone2() {
		if (textPhone2 == null) {
			textPhone2 = (HtmlInputText) findComponentInRoot("textPhone2");
		}
		return textPhone2;
	}
	protected HtmlInputText getTextEmail2() {
		if (textEmail2 == null) {
			textEmail2 = (HtmlInputText) findComponentInRoot("textEmail2");
		}
		return textEmail2;
	}
	protected HtmlInputHidden getHiddenUniqueRequestNumber() {
		if (hiddenUniqueRequestNumber == null) {
			hiddenUniqueRequestNumber = (HtmlInputHidden) findComponentInRoot("hiddenUniqueRequestNumber");
		}
		return hiddenUniqueRequestNumber;
	}
	protected HtmlCommandExButton getButtonSaveAccessRequest() {
		if (buttonSaveAccessRequest == null) {
			buttonSaveAccessRequest = (HtmlCommandExButton) findComponentInRoot("buttonSaveAccessRequest");
		}
		return buttonSaveAccessRequest;
	}
	protected HtmlMessage getMessage2() {
		if (message2 == null) {
			message2 = (HtmlMessage) findComponentInRoot("message2");
		}
		return message2;
	}
	protected HtmlMessage getMessage3() {
		if (message3 == null) {
			message3 = (HtmlMessage) findComponentInRoot("message3");
		}
		return message3;
	}
	protected HtmlMessage getMessage4() {
		if (message4 == null) {
			message4 = (HtmlMessage) findComponentInRoot("message4");
		}
		return message4;
	}
	protected HtmlMessage getMessage5() {
		if (message5 == null) {
			message5 = (HtmlMessage) findComponentInRoot("message5");
		}
		return message5;
	}
	protected HtmlMessage getMessage11() {
		if (message11 == null) {
			message11 = (HtmlMessage) findComponentInRoot("message11");
		}
		return message11;
	}
	protected HtmlMessage getMessage12() {
		if (message12 == null) {
			message12 = (HtmlMessage) findComponentInRoot("message12");
		}
		return message12;
	}
	protected HtmlOutputSeparator getSeparator2() {
		if (separator2 == null) {
			separator2 = (HtmlOutputSeparator) findComponentInRoot("separator2");
		}
		return separator2;
	}
	protected HtmlMessage getMessage1() {
		if (message1 == null) {
			message1 = (HtmlMessage) findComponentInRoot("message1");
		}
		return message1;
	}
	protected HtmlMessage getMessage6() {
		if (message6 == null) {
			message6 = (HtmlMessage) findComponentInRoot("message6");
		}
		return message6;
	}
	protected HtmlMessage getMessage7() {
		if (message7 == null) {
			message7 = (HtmlMessage) findComponentInRoot("message7");
		}
		return message7;
	}
	protected HtmlMessage getMessage13() {
		if (message13 == null) {
			message13 = (HtmlMessage) findComponentInRoot("message13");
		}
		return message13;
	}
	protected HtmlMessage getMessage14() {
		if (message14 == null) {
			message14 = (HtmlMessage) findComponentInRoot("message14");
		}
		return message14;
	}
	protected HtmlMessage getMessage8() {
		if (message8 == null) {
			message8 = (HtmlMessage) findComponentInRoot("message8");
		}
		return message8;
	}
}