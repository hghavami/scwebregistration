/*
 * Created on Jun 24, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.gannett.usatoday.SCWebRegistration.integration;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.gannett.usatoday.SCWebRegistration.to.AccessRequestTO;

/**
 * @author aeast
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SCAccessRequestDAO extends USATodayDAO {

	public void insertAccessRequest(AccessRequestTO request) throws Exception {
        Connection conn = null;
    	try {
    	    
    		conn = this.getDBConnection();
        
    		java.sql.Statement statement = conn.createStatement();
    		
    		StringBuffer sql = new StringBuffer("insert into SC_AccessRequest (authCode, locationName, locationID, locationAddress1, locationCity, locationState, locationZip, " +
    				"name1, phone1, email1, name2, phone2, email2, name3, phone3, email3, name4, phone4, email4, clientIP) values (");
		
    		// authorizationCode, locationName, locationID, locationAddress1, locationCity, locationState, locationZip,
    		if (request.getAuthorizationCode() != null){
    		    sql.append("'").append(request.getAuthorizationCode().trim()).append("',");
    		}
    		else {
    		    sql.append("null").append(",");
    		}
    		
    		if (request.getLocationName() != null){
    		    sql.append("'").append(request.getLocationName().trim()).append("',");
    		}
    		else {
    		    sql.append("null").append(",");
    		}

    		if (request.getLocationID() != null){
    		    sql.append("'").append(request.getLocationID().trim()).append("',");
    		}
    		else {
    		    sql.append("null").append(",");
    		}
    		
    		if (request.getLocationAddress1() != null){
    		    sql.append("'").append(request.getLocationAddress1().trim()).append("',");
    		}
    		else {
    		    sql.append("null").append(",");
    		}

    		if (request.getLocationCity() != null){
    		    sql.append("'").append(request.getLocationCity().trim()).append("',");
    		}
    		else {
    		    sql.append("null").append(",");
    		}

    		if (request.getLocationState() != null){
    		    sql.append("'").append(request.getLocationState().trim()).append("',");
    		}
    		else {
    		    sql.append("null").append(",");
    		}
    		
    		if (request.getLocationZip() != null){
    		    sql.append("'").append(request.getLocationZip().trim()).append("',");
    		}
    		else {
    		    sql.append("null").append(",");
    		}

    		// name1, phone1, email1, name2, phone2, email2, name3, phone3, email3, name4, phone4, email4, clientIP, insert_timestamp
    		if (request.getName1() != null){
    		    sql.append("'").append(request.getName1().trim()).append("',");
    		}
    		else {
    		    sql.append("null").append(",");
    		}

    		if (request.getPhone1() != null){
    		    sql.append("'").append(request.getPhone1().trim()).append("',");
    		}
    		else {
    		    sql.append("null").append(",");
    		}

    		if (request.getEmail1() != null){
    		    sql.append("'").append(request.getEmail1().trim()).append("',");
    		}
    		else {
    		    sql.append("null").append(",");
    		}

    		if (request.getName2() != null){
    		    sql.append("'").append(request.getName2().trim()).append("',");
    		}
    		else {
    		    sql.append("null").append(",");
    		}

    		if (request.getPhone2() != null){
    		    sql.append("'").append(request.getPhone2().trim()).append("',");
    		}
    		else {
    		    sql.append("null").append(",");
    		}

    		if (request.getEmail2() != null){
    		    sql.append("'").append(request.getEmail2().trim()).append("',");
    		}
    		else {
    		    sql.append("null").append(",");
    		}

    		if (request.getName3() != null){
    		    sql.append("'").append(request.getName3().trim()).append("',");
    		}
    		else {
    		    sql.append("null").append(",");
    		}

    		if (request.getPhone3() != null){
    		    sql.append("'").append(request.getPhone3().trim()).append("',");
    		}
    		else {
    		    sql.append("null").append(",");
    		}

    		if (request.getEmail3() != null){
    		    sql.append("'").append(request.getEmail3().trim()).append("',");
    		}
    		else {
    		    sql.append("null").append(",");
    		}

    		if (request.getName4() != null){
    		    sql.append("'").append(request.getName4().trim()).append("',");
    		}
    		else {
    		    sql.append("null").append(",");
    		}

    		if (request.getPhone4() != null){
    		    sql.append("'").append(request.getPhone4().trim()).append("',");
    		}
    		else {
    		    sql.append("null").append(",");
    		}

    		if (request.getEmail4() != null){
    		    sql.append("'").append(request.getEmail4().trim()).append("',");
    		}
    		else {
    		    sql.append("null").append(",");
    		}

    		if (request.getClientIP() != null) {
    			sql.append("'").append(request.getClientIP().trim()).append("'");
    		}
    		else {
    			sql.append("null");
    		}

    		// insert timestamp
    		//Timestamp ts = new Timestamp(System.currentTimeMillis());
    		//sql.append("'").append(ts.toString());
    		sql.append(")");

    		
    		//execute the SQL
    		int rowsAffected = statement.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS );

    		if (rowsAffected != 1) {
    		    throw new Exception("AccessRequest Failed to Insert Or other error and yet no exception condition exists: Number rows affected: " + rowsAffected);
    		}

    		ResultSet rs = statement.getGeneratedKeys();
		
    		rs.next();
    		long primaryKey = rs.getLong(1);
		
    		request.setPrimaryKey(primaryKey);
		
    		statement.close();

    	}
    	catch (Exception e) {
            System.out.println("SCAccessRequestDAO : insertAccessRequest() " + e.getMessage());
            throw e;
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        
    }
}
