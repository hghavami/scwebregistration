/*
 * Created on Jun 24, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.gannett.usatoday.SCWebRegistration.to;

import java.io.Serializable;

import com.gannett.usatoday.SCWebRegistration.integration.USATodayDAO;

/**
 * @author aeast
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class AccessRequestTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5929555944207604731L;

	long primaryKey = 0;
	
	private String authorizationCode = null;
	private String locationName = null;
	private String locationID = null;
	private String locationAddress1 = null;
	private String locationCity = null;
	private String locationState = null;
	private String locationZip = null;
	private String name1 = null;
	private String phone1 = null;
	private String email1 = null;
	private String name2 = null;
	private String phone2 = null;
	private String email2 = null;
	private String name3 = null;
	private String phone3 = null;
	private String email3 = null;
	private String name4 = null;
	private String phone4 = null;
	private String email4 = null;
	private String clientIP = null;
	
	/**
	 * 
	 */
	public AccessRequestTO() {
		super();
		
	}
	
	
	/**
	 * @return Returns the authorizationCode.
	 */
	public String getAuthorizationCode() {
		return authorizationCode;
	}
	/**
	 * @param authorizationCode The authorizationCode to set.
	 */
	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = USATodayDAO.escapeApostrophes(authorizationCode);
	}
	/**
	 * @return Returns the clientIP.
	 */
	public String getClientIP() {
		return clientIP;
	}
	/**
	 * @param clientIP The clientIP to set.
	 */
	public void setClientIP(String clientIP) {
		this.clientIP = USATodayDAO.escapeApostrophes(clientIP);
	}
	/**
	 * @return Returns the email1.
	 */
	public String getEmail1() {
		return email1;
	}
	/**
	 * @param email1 The email1 to set.
	 */
	public void setEmail1(String email1) {
		this.email1 = USATodayDAO.escapeApostrophes(email1);
	}
	/**
	 * @return Returns the email2.
	 */
	public String getEmail2() {
		return email2;
	}
	/**
	 * @param email2 The email2 to set.
	 */
	public void setEmail2(String email2) {
		this.email2 = USATodayDAO.escapeApostrophes(email2);
	}
	/**
	 * @return Returns the email3.
	 */
	public String getEmail3() {
		return email3;
	}
	/**
	 * @param email3 The email3 to set.
	 */
	public void setEmail3(String email3) {
		this.email3 = USATodayDAO.escapeApostrophes(email3);
	}
	/**
	 * @return Returns the email4.
	 */
	public String getEmail4() {
		return email4;
	}
	/**
	 * @param email4 The email4 to set.
	 */
	public void setEmail4(String email4) {
		this.email4 = USATodayDAO.escapeApostrophes(email4);
	}
	/**
	 * @return Returns the locationAddress1.
	 */
	public String getLocationAddress1() {
		return locationAddress1;
	}
	/**
	 * @param locationAddress1 The locationAddress1 to set.
	 */
	public void setLocationAddress1(String locationAddress1) {
		this.locationAddress1 = USATodayDAO.escapeApostrophes(locationAddress1);
	}
	/**
	 * @return Returns the locationID.
	 */
	public String getLocationID() {
		return locationID;
	}
	/**
	 * @param locationID The locationID to set.
	 */
	public void setLocationID(String locationID) {
		this.locationID = USATodayDAO.escapeApostrophes(locationID);
	}
	/**
	 * @return Returns the locationName.
	 */
	public String getLocationName() {
		return locationName;
	}
	/**
	 * @param locationName The locationName to set.
	 */
	public void setLocationName(String locationName) {
		this.locationName = USATodayDAO.escapeApostrophes(locationName);
	}
	/**
	 * @return Returns the locationCity.
	 */
	public String getLocationCity() {
		return locationCity;
	}
	/**
	 * @param locationCity The locationCity to set.
	 */
	public void setLocationCity(String locationCity) {
		this.locationCity = USATodayDAO.escapeApostrophes(locationCity);
	}
	/**
	 * @return Returns the locationState.
	 */
	public String getLocationState() {
		return locationState;
	}
	/**
	 * @param locationState The locationState to set.
	 */
	public void setLocationState(String locationState) {
		this.locationState = USATodayDAO.escapeApostrophes(locationState);
	}
	/**
	 * @return Returns the locationZip.
	 */
	public String getLocationZip() {
		return locationZip;
	}
	/**
	 * @param locationZip The locationZip to set.
	 */
	public void setLocationZip(String locationZip) {
		this.locationZip = USATodayDAO.escapeApostrophes(locationZip);
	}
	/**
	 * @return Returns the name1.
	 */
	public String getName1() {
		return name1;
	}
	/**
	 * @param name1 The name1 to set.
	 */
	public void setName1(String name1) {
		this.name1 = USATodayDAO.escapeApostrophes(name1);
	}
	/**
	 * @return Returns the name2.
	 */
	public String getName2() {
		return name2;
	}
	/**
	 * @param name2 The name2 to set.
	 */
	public void setName2(String name2) {
		this.name2 = USATodayDAO.escapeApostrophes(name2);
	}
	/**
	 * @return Returns the name3.
	 */
	public String getName3() {
		return name3;
	}
	/**
	 * @param name3 The name3 to set.
	 */
	public void setName3(String name3) {
		this.name3 = USATodayDAO.escapeApostrophes(name3);
	}
	/**
	 * @return Returns the name4.
	 */
	public String getName4() {
		return name4;
	}
	/**
	 * @param name4 The name4 to set.
	 */
	public void setName4(String name4) {
		this.name4 = USATodayDAO.escapeApostrophes(name4);
	}
	/**
	 * @return Returns the phone1.
	 */
	public String getPhone1() {
		return phone1;
	}
	/**
	 * @param phone1 The phone1 to set.
	 */
	public void setPhone1(String phone1) {
		this.phone1 = USATodayDAO.escapeApostrophes(phone1);
	}
	/**
	 * @return Returns the phone2.
	 */
	public String getPhone2() {
		return phone2;
	}
	/**
	 * @param phone2 The phone2 to set.
	 */
	public void setPhone2(String phone2) {
		this.phone2 = USATodayDAO.escapeApostrophes(phone2);
	}
	/**
	 * @return Returns the phone3.
	 */
	public String getPhone3() {
		return phone3;
	}
	/**
	 * @param phone3 The phone3 to set.
	 */
	public void setPhone3(String phone3) {
		this.phone3 = USATodayDAO.escapeApostrophes(phone3);
	}
	/**
	 * @return Returns the phone4.
	 */
	public String getPhone4() {
		return phone4;
	}
	/**
	 * @param phone4 The phone4 to set.
	 */
	public void setPhone4(String phone4) {
		this.phone4 = USATodayDAO.escapeApostrophes(phone4);
	}
	/**
	 * @return Returns the primaryKey.
	 */
	public long getPrimaryKey() {
		return primaryKey;
	}
	/**
	 * @param primaryKey The primaryKey to set.
	 */
	public void setPrimaryKey(long primaryKey) {
		this.primaryKey = primaryKey;
	}
}
